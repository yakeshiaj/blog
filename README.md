# README


# Planning our application
  1. Answer Questions
    - What are we building?
    - Who are we building it for?
    - what features do we need to have?
  2. User Stories
  3. Model our Data
  4. Layouts

## Questions
  1. What are we building?
    We are building a personal site. A place where we can blog, share examples of our work, and have people contact us.
  2. Who are we building it for?
    We are building it for ourselves and the community. Sharing what we are learning by blogging is a great way to learn for ourselves while teaching others in the process. Show potential employers that we have knowledge and experience.
  3. what features do we need to have?
    - Articles
        - Create / Edit / Destroy
        - Markdown
        - Syntax highlighting
        - Comments (Disqus)
    - Projects
        - Create / Edit / Destroy
    - Contact  
        - Contact form
        - SendGrid: Email Delivery Service
    - User (Devise)

## User Stories
  As a ______, I want to be able to ______, so that ______.
  
  - As a user, I want to be able to create posts, so that I can share   what I am learning on my blog.
  - As a user, I want to be able to perform CRUD operations, so that I can manage my blog.
  - As a user, I want to be able to write posts in markdown format, so that writing posts can be efficient and user-friendly.
  - As a user, I want to be able to highlight the various syntax of code blocks that I share on my blog.
  - As a user, I want to show the visitors and potential employers examples of my work and/or apps I have built.
  - As a user, I want to be able to have visitors contact me through a form on my site.
  - As a user, I want visitors to be able to leave comments on my posts.

## Modeling our Data
  **Post**
  
	- title:string
	- content:string
	- comments:string
  **Projects**
  
	- title:string
	- description:string
 	- link:string
	
  **User**

## Layouts
	- Home
	- Articles#index
	- Project#index
	- Project#Show
	- Contact

